Minimal reactive test runner for JavaScript
==========================================

__This library is experimenal__

I was looking for a simple test runner that also works in PhantomJS scripts but
could not find any. This is a minimal test runner build using reactive streams
(also called Observables). You are free to use any assertion library. Testing of
asynchronous code can be managed with any promise library.

Install with `npm install rxtestrunner` or clone this repo.

Here is an example of how it is used usage:

```
var assert = ( typeof phantom === 'object' && require('chai').assert ) ||
             ( typeof process === 'object' && require('assert') ) ||
             console.assert;

var TestObservable = require('rxtestrunner');

test1 = function() {
  assert.ok(true, 'this should not throw an error');
  assert.ok(false, 'this should throw an error');
};

test2 = function() {
  assert.ok(true, 'this should not throw an error');
  assert.ok(false, 'this should throw an error');
};

(TestObservable.test(test1))
.merge(TestObservable.test(test2))
  .map(TestObservable.basicFormat)
  .map(console.log.bind(console))
```


Suggestions and contributions and welcome
----------------------------------------

Just create a github issue or pull request.


License
-------

MIT
