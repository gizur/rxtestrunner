var assert = ( typeof phantom === 'object' && require('chai').assert ) ||
             ( typeof process === 'object' && require('assert') ) ||
             console.assert;

var TestObservable = require('./index.js');

test1 = function() {
  assert.ok(true, 'this should not throw an error');
  assert.ok(false, 'this should throw an error');
};

test2 = function() {
  TestObservable.immediate(function() {
    assert.ok(true, 'this should not throw an error');
    assert.ok(false, 'this should throw an error');
  });
};

test3 = function() {
  assert.ok(true, 'this should not throw an error');
  assert.ok(false, 'this should throw an error');
};

test4 = function() {
  assert.ok(true, 'this should not throw an error');
  assert.ok(true, 'this should not throw an error');
};


(TestObservable.test(test1))
.merge(TestObservable.test(test2))
.merge(TestObservable.test(test3))
.merge(TestObservable.test(test4))
  .map(TestObservable.basicFormat)
  .map(console.log.bind(console))
